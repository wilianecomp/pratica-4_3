/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.convert;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author wili
 */
@FacesConverter("cpfConverter")
public class CPFConverter implements Converter{
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value)
        throws ConverterException {
        return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value)
        throws ConverterException {
        //Long num = Long.valueOf(value.toString());
        String num = value.toString();
        String um,dois,tres,quatro;
        um = (String) num.subSequence(0, 3);
        dois = (String) num.subSequence(3, 6);
        tres = (String) num.subSequence(6, 9);
        quatro = (String) num.subSequence(9, 11);
        num = um + "." +dois+"."+tres+"-"+quatro;
        return num;
    }
}
